
# raytracer

A raytracer written in Rust.

For details and images, take a look at the [post on my website](https://maxkl.de/projects/graphics/raytracer/).

This is just a library, see [maxkl2/raytracer_ui](https://gitlab.com/maxkl2/raytracer_ui) for a native user interface and https://projects.maxkl.de/raytracer/ ([maxkl2/raytracer_web](https://gitlab.com/maxkl2/raytracer_web)) for a web frontend using WebAssembly.
