
use std::error::Error;
use std::path::PathBuf;
use std::time::Instant;

use serde::{Serialize, Deserialize, Deserializer};
use cgmath::{Vector3, InnerSpace, Zero, EuclideanSpace, Vector2};

use crate::ray::{Hit, Ray};
use crate::asset_loader;
use crate::aabb::AABB;
use crate::kd_tree::{KDTree, KDTreeOptions, KDTreeData, KDTreeHit};

pub struct TriangleHit {
    distance: f32,
    u: f32,
    v: f32,
}

impl KDTreeHit for TriangleHit {
    fn distance(&self) -> f32 {
        self.distance
    }
}

fn intersect_triangle(ray: &Ray, v0: &Vector3<f32>, v1: &Vector3<f32>, v2: &Vector3<f32>) -> Option<TriangleHit> {
    // Möller-Trumbore ray-triangle intersection algorithm

    let v0v1: Vector3<_> = v1 - v0;
    let v0v2: Vector3<_> = v2 - v0;
    let pvec = ray.direction.cross(v0v2);
    let det = v0v1.dot(pvec);

    if det.abs() < f32::EPSILON {
        return None;
    }

    let inv_det = 1.0 / det;

    let tvec = ray.origin.to_vec() - v0;
    let u = tvec.dot(pvec) * inv_det;
    if u < 0.0 || u > 1.0 {
        return None;
    }

    let qvec = tvec.cross(v0v1);
    let v = ray.direction.dot(qvec) * inv_det;
    if v < 0.0 || u + v > 1.0 {
        return None;
    }

    let t = v0v2.dot(qvec) * inv_det;

    if t < 0.0 {
        return None;
    }

    Some(TriangleHit {
        distance: t,
        u,
        v,
    })
}

#[derive(Clone)]
pub struct IndexedTriangle {
    pub position_indices: (usize, usize, usize),
    pub normal_indices: Option<(usize, usize, usize)>,
    pub tex_coords_indices: Option<(usize, usize, usize)>,
}

#[derive(Clone)]
pub struct IndexedMeshData {
    pub vertex_positions: Vec<(f32, f32, f32)>,
    pub vertex_normals: Vec<(f32, f32, f32)>,
    pub vertex_tex_coords: Vec<(f32, f32)>,
    pub triangles: Vec<IndexedTriangle>,
}

impl IndexedMeshData {
    fn get_vertex_position(&self, index: usize) -> &Vector3<f32> {
        (&self.vertex_positions[index]).into()
    }

    fn get_vertex_normal(&self, index: usize) -> &Vector3<f32> {
        (&self.vertex_normals[index]).into()
    }

    fn get_vertex_tex_coords(&self, index: usize) -> &Vector2<f32> {
        (&self.vertex_tex_coords[index]).into()
    }
}

impl KDTreeData for IndexedMeshData {
    type Hit = TriangleHit;

    fn primitive_count(&self) -> usize {
        self.triangles.len()
    }

    fn bounding_box(&self, triangle_index: usize) -> AABB {
        let triangle = &self.triangles[triangle_index];
        let v0 = self.get_vertex_position(triangle.position_indices.0);
        let v1 = self.get_vertex_position(triangle.position_indices.1);
        let v2 = self.get_vertex_position(triangle.position_indices.2);

        AABB::from_triangle(v0, v1, v2)
    }

    fn intersect(&self, triangle_index: usize, ray: &Ray) -> Option<TriangleHit> {
        let triangle = &self.triangles[triangle_index];
        let v0 = self.get_vertex_position(triangle.position_indices.0);
        let v1 = self.get_vertex_position(triangle.position_indices.1);
        let v2 = self.get_vertex_position(triangle.position_indices.2);

        intersect_triangle(ray, v0, v1, v2)
    }

    fn resolve_hit(&self, triangle_index: usize, triangle_hit: &TriangleHit, ray: &Ray) -> Hit {
        let triangle = &self.triangles[triangle_index];

        let normal = triangle.normal_indices.map_or_else(|| {
            let v0 = self.get_vertex_position(triangle.position_indices.0);
            let v1 = self.get_vertex_position(triangle.position_indices.1);
            let v2 = self.get_vertex_position(triangle.position_indices.2);

            // Calculate face normal from vertex positions
            (v1 - v0).cross(v2 - v0).normalize()
        }, |normal_indices| {
            let n0 = self.get_vertex_normal(normal_indices.0);
            let n1 = self.get_vertex_normal(normal_indices.1);
            let n2 = self.get_vertex_normal(normal_indices.2);

            // Interpolate vertex normals using the barycentric coordinates of the hit point
            (1.0 - triangle_hit.u - triangle_hit.v) * n0 + triangle_hit.u * n1 + triangle_hit.v * n2
        });

        let tex_coords = triangle.tex_coords_indices.map_or_else(|| {
            Vector2::zero()
        }, |tex_coords_indices| {
            let t0 = self.get_vertex_tex_coords(tex_coords_indices.0);
            let t1 = self.get_vertex_tex_coords(tex_coords_indices.1);
            let t2 = self.get_vertex_tex_coords(tex_coords_indices.2);

            // Interpolate vertex texture coordinates using the barycentric coordinates of the hit point
            (1.0 - triangle_hit.u - triangle_hit.v) * t0 + triangle_hit.u * t1 + triangle_hit.v * t2
        });

        Hit {
            point: ray.origin + ray.direction * triangle_hit.distance,
            distance: triangle_hit.distance,
            normal,
            tex_coords,
        }
    }
}

#[derive(Clone)]
pub struct InPlaceTriangle {
    positions: (Vector3<f32>, Vector3<f32>, Vector3<f32>),
    normals: (Vector3<f32>, Vector3<f32>, Vector3<f32>),
    tex_coords: (Vector2<f32>, Vector2<f32>, Vector2<f32>),
}

#[derive(Clone)]
pub struct InPlaceMeshData {
    pub triangles: Vec<InPlaceTriangle>,
}

impl KDTreeData for InPlaceMeshData {
    type Hit = TriangleHit;

    fn primitive_count(&self) -> usize {
        self.triangles.len()
    }

    fn bounding_box(&self, triangle_index: usize) -> AABB {
        let triangle = &self.triangles[triangle_index];
        AABB::from_triangle(&triangle.positions.0, &triangle.positions.1, &triangle.positions.2)
    }

    fn intersect(&self, triangle_index: usize, ray: &Ray) -> Option<TriangleHit> {
        let triangle = &self.triangles[triangle_index];
        intersect_triangle(ray, &triangle.positions.0, &triangle.positions.1, &triangle.positions.2)
    }

    fn resolve_hit(&self, triangle_index: usize, triangle_hit: &TriangleHit, ray: &Ray) -> Hit {
        let triangle = &self.triangles[triangle_index];

        let n0 = &triangle.normals.0;
        let n1 = &triangle.normals.1;
        let n2 = &triangle.normals.2;

        // Interpolate vertex normals using the barycentric coordinates of the hit point
        let normal = (1.0 - triangle_hit.u - triangle_hit.v) * n0 + triangle_hit.u * n1 + triangle_hit.v * n2;

        let t0 = &triangle.tex_coords.0;
        let t1 = &triangle.tex_coords.1;
        let t2 = &triangle.tex_coords.2;

        // Interpolate vertex texture coordinates using the barycentric coordinates of the hit point
        let tex_coords = (1.0 - triangle_hit.u - triangle_hit.v) * t0 + triangle_hit.u * t1 + triangle_hit.v * t2;

        Hit {
            point: ray.origin + ray.direction * triangle_hit.distance,
            distance: triangle_hit.distance,
            normal,
            tex_coords,
        }
    }
}

impl From<IndexedMeshData> for InPlaceMeshData {
    fn from(indexed: IndexedMeshData) -> InPlaceMeshData {
        let mut triangles = Vec::with_capacity(indexed.triangles.len());

        for indexed_triangle in &indexed.triangles {
            let positions = (
                indexed.get_vertex_position(indexed_triangle.position_indices.0).clone(),
                indexed.get_vertex_position(indexed_triangle.position_indices.1).clone(),
                indexed.get_vertex_position(indexed_triangle.position_indices.2).clone(),
            );

            let normals = indexed_triangle.normal_indices.map_or_else(|| {
                let v0 = indexed.get_vertex_position(indexed_triangle.position_indices.0);
                let v1 = indexed.get_vertex_position(indexed_triangle.position_indices.1);
                let v2 = indexed.get_vertex_position(indexed_triangle.position_indices.2);

                // Calculate face normal from vertex positions
                let normal: Vector3<f32> = (v1 - v0).cross(v2 - v0).normalize();

                (normal, normal, normal)
            }, |normal_indices| (
                indexed.get_vertex_normal(normal_indices.0).clone(),
                indexed.get_vertex_normal(normal_indices.1).clone(),
                indexed.get_vertex_normal(normal_indices.2).clone()
            ));

            let tex_coords = indexed_triangle.tex_coords_indices.map_or_else(|| {
                (Vector2::zero(), Vector2::zero(), Vector2::zero())
            }, |tex_coords_indices| (
                indexed.get_vertex_tex_coords(tex_coords_indices.0).clone(),
                indexed.get_vertex_tex_coords(tex_coords_indices.1).clone(),
                indexed.get_vertex_tex_coords(tex_coords_indices.2).clone()
            ));

            triangles.push(InPlaceTriangle {
                positions,
                normals,
                tex_coords,
            });
        }

        InPlaceMeshData {
            triangles,
        }
    }
}

fn default_debug() -> bool {
    false
}

#[derive(Serialize, Deserialize)]
struct DeserializableMesh {
    path: PathBuf,
    #[serde(default = "default_debug")]
    debug: bool,
}

impl From<Mesh> for DeserializableMesh {
    fn from(mesh: Mesh) -> DeserializableMesh {
        DeserializableMesh {
            path: mesh.path,
            debug: mesh.debug,
        }
    }
}

#[derive(Clone, Serialize)]
#[serde(into = "DeserializableMesh")]
pub struct Mesh {
    path: PathBuf,
    kdtree: KDTree<InPlaceMeshData>,
    debug: bool,
}

impl<'de> Deserialize<'de> for Mesh {
    fn deserialize<D>(deserializer: D) -> Result<Mesh, D::Error>
        where
            D: Deserializer<'de>
    {
        let dmesh = DeserializableMesh::deserialize(deserializer)?;
        Self::load(dmesh.path.clone(), dmesh.debug).map_err(|err| {
            serde::de::Error::custom(format!("Unable to open mesh file \"{}\": {}", dmesh.path.display(), err))
        })
    }
}

impl Mesh {
    pub fn new(path: PathBuf, data: InPlaceMeshData, debug: bool) -> Mesh {
        let start = Instant::now();
        let kdtree = KDTree::build(data, &KDTreeOptions {
            debug,
            ..KDTreeOptions::default()
        });
        let duration = start.elapsed().as_secs_f64();
        if debug {
            let max_depth = kdtree.max_depth();
            println!("K-D tree for {} built in {} s with a maximum depth of {} nodes", path.display(), duration, max_depth);
        }

        Mesh {
            path,
            kdtree,
            debug,
        }
    }

    pub fn load(path: PathBuf, debug: bool) -> Result<Mesh, Box<dyn Error>> {
        let a = asset_loader::get_instance();
        let data = a.load_obj(&path)?;
        Ok(Mesh::new(path, data.into(), debug))
    }

    pub fn intersect(&self, ray: &Ray) -> Option<Hit> {
        self.kdtree.intersect(ray)
    }
}
