
use std::mem;

use cgmath::Vector3;

use crate::math_util::Axis;
use crate::aabb::AABB;
use crate::ray::{Ray, Hit};

pub struct KDTreeOptions {
    pub max_depth: Option<usize>,
    pub max_leaf_size: usize,
    pub debug: bool,
}

impl Default for KDTreeOptions {
    fn default() -> Self {
        KDTreeOptions {
            max_depth: None,
            max_leaf_size: 16,
            debug: false,
        }
    }
}

pub trait KDTreeHit {
    fn distance(&self) -> f32;
}

pub trait KDTreeData {
    type Hit: KDTreeHit;

    fn primitive_count(&self) -> usize;

    fn bounding_box(&self, index: usize) -> AABB;

    fn intersect(&self, index: usize, ray: &Ray) -> Option<Self::Hit>;

    fn resolve_hit(&self, index: usize, hit: &Self::Hit, ray: &Ray) -> Hit;
}

#[derive(Clone)]
struct KDTreeNode {
    /// Leaf node: the two LSBs are 0b11, the 30 MSBs hold the number of primitives in this node
    /// Inner node: the two LSBs store the split axis (0-2), the 30 MSBs hold the index of the second child node
    first_field: u32,
    /// Leaf node: the index of the first primitive in `linear_primitive_indices`
    /// Inner node: the split position as f32 (using mem::transmute())
    second_field: u32,
}

impl KDTreeNode {
    fn new_leaf(primitive_count: u32, primitives_start_index: u32) -> KDTreeNode {
        KDTreeNode {
            first_field: primitive_count.checked_shl(2).unwrap() | 0x3,
            second_field: primitives_start_index,
        }
    }

    fn new_inner(above_child_index: u32, split_axis: Axis, split_position: f32) -> KDTreeNode {
        KDTreeNode {
            first_field: above_child_index.checked_shl(2).unwrap() | split_axis as u32,
            second_field: unsafe { mem::transmute(split_position) },
        }
    }

    fn is_inner(&self) -> bool {
        self.first_field & 0x3 != 0x3
    }

    fn split_axis(&self) -> Axis {
        (self.first_field & 0x3).into()
    }

    fn above_child_index(&self) -> u32 {
        self.first_field >> 2
    }

    fn set_above_child_index(&mut self, above_child_index: u32) {
        self.first_field = above_child_index.checked_shl(2).unwrap() | self.first_field & 0x3;
    }

    fn primitive_count(&self) -> u32 {
        self.first_field >> 2
    }

    fn primitives_start_index(&self) -> u32 {
        self.second_field
    }

    fn split_position(&self) -> f32 {
        unsafe {
            mem::transmute(self.second_field)
        }
    }
}

#[derive(Clone)]
pub struct KDTree<D: KDTreeData> {
    /// All nodes are stored depth-first in this vector to improve traversal speed
    nodes: Vec<KDTreeNode>,
    linear_primitive_indices: Vec<usize>,
    bounding_box: AABB,
    data: D,
    debug: bool,
    intersect_stack_capacity: usize,
}

/// Edge of a bounding box projected onto an axis
struct BoundEdge {
    position: f32,
    primitive_index: usize,
    is_end: bool,
}

/// Node that still has to be traversed during K-D tree intersection test
struct ToDoItem {
    node_index: usize,
    t_min: f32,
    t_max: f32,
}

impl<D: KDTreeData> KDTree<D> {
    pub fn build(data: D, options: &KDTreeOptions) -> KDTree<D> {
        let primitive_count = data.primitive_count();

        // Formula taken from "Physically Based Rendering: From Theory To Implementation"
        let max_depth = options.max_depth
            .unwrap_or_else(|| 8 + (1.3 * (primitive_count as f32).log2()).round() as usize);

        let mut root_bounding_box = AABB::empty();
        let mut primitive_bounding_boxes = Vec::with_capacity(primitive_count);
        for primitive_index in 0..primitive_count {
            let bounding_box = data.bounding_box(primitive_index);
            root_bounding_box = root_bounding_box.union(&bounding_box);
            primitive_bounding_boxes.push(bounding_box);
        }

        // All required working memory is allocated up front

        // Initialize with indices of all primitives
        let mut indices_below: Vec<_> = (0..primitive_count).collect();
        // Reserve size for worst case
        let mut indices_above = vec![0; (max_depth + 1) * primitive_count];
        let mut edges = Vec::with_capacity(primitive_count * 2);

        let mut nodes = Vec::new();
        let mut linear_primitive_indices = Vec::new();

        Self::build_node(
            &mut nodes,
            &mut linear_primitive_indices,
            &mut indices_below,
            &mut indices_above,
            // The initial set of primitives is passed in `indices_below`
            false,
            primitive_count,
            &root_bounding_box,
            &primitive_bounding_boxes,
            max_depth,
            options,
            &mut edges,
        );

        nodes.shrink_to_fit();
        linear_primitive_indices.shrink_to_fit();

        let max_depth = Self::max_depth_recursive(&nodes, 0);
        let intersect_stack_capacity = (max_depth as f32 * 0.65).round() as usize;

        KDTree {
            nodes,
            linear_primitive_indices,
            bounding_box: root_bounding_box,
            data,
            debug: options.debug,
            intersect_stack_capacity,
        }
    }

    /// Construct a new node in place
    ///
    /// Arguments:
    ///
    /// * `nodes`: All nodes in depth-first, left-to-right order
    /// * `linear_primitive_indices`: The indices of all primitives; all indices of one leaf node are grouped together
    /// * `primitive_indices_below`: Heap space for nodes below the previous split
    /// * `primitive_indices_above`: Heap space for nodes above the previous split
    /// * `is_above`: Whether `primitive_indices_below` or `primitive_indices_above` contains the primitive indices for this node
    /// * `primitive_count`: Number of primitives in this node, also determines how many items of `primitive_indices_below` or `primitive_indices_above` are valid
    /// * `node_bounding_box`: Bounding box of all primitives in this node
    /// * `primitive_bounding_boxes`: Bounding boxes of all primitives
    /// * `depth_remaining`: Decremented with each level of recursion
    /// * `options`: Build options
    /// * `edges`: Pre-allocated heap space for bounding box edges
    fn build_node(
        nodes: &mut Vec<KDTreeNode>,
        linear_primitive_indices: &mut Vec<usize>,
        primitive_indices_below: &mut [usize],
        primitive_indices_above: &mut [usize],
        is_above: bool,
        primitive_count: usize,
        node_bounding_box: &AABB,
        primitive_bounding_boxes: &[AABB],
        depth_remaining: usize,
        options: &KDTreeOptions,
        edges: &mut Vec<BoundEdge>,
    ) {
        let primitive_indices = if is_above {
            &primitive_indices_above[..primitive_count]
        } else {
            &primitive_indices_below[..primitive_count]
        };

        if primitive_count <= options.max_leaf_size || depth_remaining == 0 {
            let start_index = linear_primitive_indices.len();
            linear_primitive_indices.extend_from_slice(primitive_indices);
            nodes.push(KDTreeNode::new_leaf(primitive_count as u32, start_index as u32));

            return;
        }

        let split_axis = node_bounding_box.maximum_extent();

        edges.clear();
        for &primitive_index in primitive_indices {
            let bounding_box = &primitive_bounding_boxes[primitive_index];
            edges.push(BoundEdge { position: bounding_box.min[split_axis], primitive_index, is_end: false });
            edges.push(BoundEdge { position: bounding_box.max[split_axis], primitive_index, is_end: true });
        }

        edges.sort_unstable_by(|a, b| {
            a.position.partial_cmp(&b.position).unwrap()
        });

        // TODO: replace median with SAH
        let split_position = (edges[edges.len() / 2].position + edges[edges.len() / 2 + 1].position) * 0.5;

        let mut n_below = 0;
        let mut n_above = 0;

        // Edges are sorted by their position -> edges below split come first
        let mut i = 0;
        while i < edges.len() && edges[i].position <= split_position {
            // All primitives whose lower edge is below the split
            if !edges[i].is_end {
                primitive_indices_below[n_below] = edges[i].primitive_index;
                n_below += 1;
            }
            i += 1;
        }
        // The remaining edges are all above the split
        while i < edges.len() {
            // All primitives whose upper edge is above the split
            if edges[i].is_end {
                primitive_indices_above[n_above] = edges[i].primitive_index;
                n_above += 1;
            }
            i += 1;
        }

        let node_index = nodes.len();
        // We don't know the index of the second child node yet
        nodes.push(KDTreeNode::new_inner(0, split_axis, split_position));

        let mut bounding_box_below = node_bounding_box.clone();
        bounding_box_below.max[split_axis] = split_position;
        Self::build_node(
            nodes,
            linear_primitive_indices,
            primitive_indices_below,
            // The first `n_above` items of `primitive_indices_above` need to be preserved for construction of the second child node
            &mut primitive_indices_above[n_above..],
            false,
            n_below,
            &bounding_box_below,
            primitive_bounding_boxes,
            depth_remaining - 1,
            options,
            edges,
        );

        // Update index of the second child node now that we know it
        let second_child_index = nodes.len();
        nodes[node_index].set_above_child_index(second_child_index as u32);

        let mut bounding_box_above = node_bounding_box.clone();
        bounding_box_above.min[split_axis] = split_position;
        Self::build_node(
            nodes,
            linear_primitive_indices,
            primitive_indices_below,
            primitive_indices_above,
            true,
            n_above,
            &bounding_box_above,
            primitive_bounding_boxes,
            depth_remaining - 1,
            options,
            edges,
        );
    }

    fn max_depth_recursive(nodes: &[KDTreeNode], node_index: usize) -> usize {
        let node = &nodes[node_index];
        if node.is_inner() {
            let first_child_index = node_index + 1;
            let second_child_index = node.above_child_index() as usize;

            let max_child_depth = usize::max(
                Self::max_depth_recursive(nodes, first_child_index),
                Self::max_depth_recursive(nodes, second_child_index)
            );

            max_child_depth + 1
        } else {
            1
        }
    }

    pub fn max_depth(&self) -> usize {
        Self::max_depth_recursive(&self.nodes, 0)
    }

    pub fn intersect(&self, ray: &Ray) -> Option<Hit> {
        if let Some((bb_t_min, bb_t_max)) = self.bounding_box.intersects_p(ray) {
            let mut todo_stack = Vec::with_capacity(self.intersect_stack_capacity);

            // Push root node onto stack
            todo_stack.push(ToDoItem {
                node_index: 0,
                t_min: bb_t_min,
                t_max: bb_t_max,
            });

            let mut nearest_hit: Option<(usize, D::Hit)> = None;

            // Number of nodes we had to look up, for debugging purposes
            let mut lookups = 1;

            let inv_dir: Vector3<f32> = 1.0 / ray.direction;

            while let Some(ToDoItem { node_index, t_min, t_max }) = todo_stack.pop() {
                // Bail out if this node is behind the nearest hit that was found so far
                if let Some((_, nearest_hit)) = &nearest_hit {
                    if nearest_hit.distance() < t_min {
                        break;
                    }
                }

                lookups += 1;

                let node = &self.nodes[node_index];
                if node.is_inner() {
                    let above_child_index = node.above_child_index() as usize;
                    let split_axis = node.split_axis();
                    let split_position = node.split_position();

                    let origin_position = ray.origin[split_axis];

                    // Find distance at which the ray intersects the split plane
                    let t_split = (split_position - origin_position) * inv_dir[split_axis];

                    // Determine which child the ray crosses first
                    let first_child_index;
                    let second_child_index;
                    if origin_position < split_position || (origin_position == split_position && ray.direction[split_axis] <= 0.0) {
                        first_child_index = node_index + 1;
                        second_child_index = above_child_index;
                    } else {
                        first_child_index = above_child_index;
                        second_child_index = node_index + 1;
                    }

                    if t_split > t_max || t_split <= 0.0 {
                        // The ray leaves this node before it intersects the second child (t_split > t_max) or
                        //  the ray points away from the splitting plane (t_split <= 0)
                        //  -> only the first child is intersected
                        todo_stack.push(ToDoItem {
                            node_index: first_child_index,
                            t_min,
                            t_max,
                        });
                    } else if t_split < t_min {
                        // The ray intersects the splitting plane before it enters the node
                        //  -> only the second child is intersected
                        todo_stack.push(ToDoItem {
                            node_index: second_child_index,
                            t_min,
                            t_max,
                        });
                    } else {
                        // Stack is LIFO -> node at `first_child_index` will be processed next
                        todo_stack.push(ToDoItem {
                            node_index: second_child_index,
                            t_min: t_split,
                            t_max,
                        });
                        todo_stack.push(ToDoItem {
                            node_index: first_child_index,
                            t_min,
                            t_max: t_split,
                        });
                    }
                } else {
                    let start_index = node.primitives_start_index() as usize;
                    let primitive_count = node.primitive_count() as usize;
                    let primitive_indices = &self.linear_primitive_indices[start_index..(start_index + primitive_count)];

                    // Test ray against all primitives in this node
                    for &primitive_index in primitive_indices {
                        if let Some(hit) = self.data.intersect(primitive_index, ray) {
                            // Update `nearest_hit` only if it really is the nearest one
                            if let Some((_, current_nearest_hit)) = &nearest_hit {
                                if hit.distance() < current_nearest_hit.distance() {
                                    nearest_hit = Some((primitive_index, hit));
                                }
                            } else {
                                nearest_hit = Some((primitive_index, hit));
                            }
                        }
                    }
                }
            }

            if self.debug {
                let mut debug_data = ray.debug_data.borrow_mut();
                debug_data.kd_tree_lookups += lookups;
            }

            nearest_hit.map(|(primitive_index, primitive_hit)| {
                self.data.resolve_hit(primitive_index, &primitive_hit, ray)
            })
        } else {
            None
        }
    }
}
